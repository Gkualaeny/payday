package com.paydayauthorization.paydayauthorization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaydayauthorizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaydayauthorizationApplication.class, args);
	}

}
