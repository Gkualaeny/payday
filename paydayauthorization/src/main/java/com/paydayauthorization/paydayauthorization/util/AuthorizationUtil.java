package com.paydayauthorization.paydayauthorization.util;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class AuthorizationUtil {
	@Bean
	private static PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}
	
	public static String passwordEncoder(String password) {
		return passwordEncoder().encode(password);
	}
	public static boolean passwordMatches(String password,String encryptedPasswordFromDb) {
        BCryptPasswordEncoder bcrypt= new BCryptPasswordEncoder();  
       return bcrypt.matches(password, encryptedPasswordFromDb);
	}

}
