package com.paydayauthorization.paydayauthorization.request;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.paydayauthorization.paydayauthorization.model.authorization.RegisterUserDTO;

import io.swagger.annotations.ApiModel;


@ApiModel
public class RequestSignUp {
	@NotNull
	@Valid
private RegisterUserDTO signUpDTO;

	public RegisterUserDTO getSignUpDTO() {
		return signUpDTO;
	}

	public void setSignUpDTO(RegisterUserDTO signUpDTO) {
		this.signUpDTO = signUpDTO;
	}

}

