package com.paydayauthorization.paydayauthorization.request;
import com.paydayauthorization.paydayauthorization.model.authorization.AuthorizationSignInDTO;

import io.swagger.annotations.ApiModel;


@ApiModel
public class RequestSignIn {

private AuthorizationSignInDTO signInDTO;

public AuthorizationSignInDTO getSignInDTO() {
	return signInDTO;
}

public void setSignInDTO(AuthorizationSignInDTO signInDTO) {
	this.signInDTO = signInDTO;
}

}
