package com.paydayauthorization.paydayauthorization.response;

import com.paydayauthorization.paydayauthorization.model.authorization.AuthorizationShowDTO;

public class ResponseAuthorization extends BaseApiResponse{

	private AuthorizationShowDTO authorizationShowDTO;

	public AuthorizationShowDTO getAuthorizationShowDTO() {
		return authorizationShowDTO;
	}

	public void setAuthorizationShowDTO(AuthorizationShowDTO authorizationShowDTO) {
		this.authorizationShowDTO = authorizationShowDTO;
	}




}
