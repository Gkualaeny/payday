package com.paydayauthorization.paydayauthorization.enums;

public enum PasswordValidateEnum {
	MIN_LENGTH("Password minnimum length can be 6 character.Please enter valid password "), 
	ALPH_NUMERIC("All of password characters must be alpha-numeric character.Please enter valid password");
	private String value;

	PasswordValidateEnum(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return value;
	}


}
