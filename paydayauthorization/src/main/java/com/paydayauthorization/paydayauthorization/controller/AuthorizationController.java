package com.paydayauthorization.paydayauthorization.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.paydayauthorization.paydayauthorization.model.authorization.AuthorizationCommandService;
import com.paydayauthorization.paydayauthorization.model.authorization.AuthorizationQueryService;
import com.paydayauthorization.paydayauthorization.model.authorization.AuthorizationShowDTO;
import com.paydayauthorization.paydayauthorization.request.RequestSignIn;
import com.paydayauthorization.paydayauthorization.request.RequestSignUp;
import com.paydayauthorization.paydayauthorization.response.ResponseAuthorization;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/Authorization")
@Api(value = "Authorization")
public class AuthorizationController {
	private AuthorizationCommandService authorizationCommandService;
	private AuthorizationQueryService authorizationQueryService;
	@Autowired
	public AuthorizationController(AuthorizationCommandService authorizationCommandService,AuthorizationQueryService authorizationQueryService) {
		this.authorizationCommandService=authorizationCommandService;
		this.authorizationQueryService=authorizationQueryService;
	}

	@ResponseStatus(HttpStatus.OK)
	@PostMapping( value="/authorization/sign/in", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseAuthorization signIn(@RequestBody @NotNull @Valid RequestSignIn request) {
		ResponseAuthorization response=new ResponseAuthorization();
		response.setAuthorizationShowDTO(authorizationQueryService.signIn(request.getSignInDTO().getEmail(), request.getSignInDTO().getPassword()));
		return response;
	}

	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "", notes = "Get data of application screens")
	@PostMapping( value="/authorization/sign/up", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseAuthorization signUp(@RequestBody @NotNull @Valid RequestSignUp request) {
		AuthorizationShowDTO authorizationShowDTO=authorizationCommandService.signUp(request.getSignUpDTO());
		ResponseAuthorization response=new ResponseAuthorization();
		response.setAuthorizationShowDTO(authorizationShowDTO);
		return response;
	}

}
