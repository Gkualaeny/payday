package com.paydayauthorization.paydayauthorization.model.authorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paydayauthorization.paydayauthorization.util.AuthorizationUtil;


@Service
public class AuthorizationQueryService implements IAuthorizationQueryService {
	private AuthorizationRepository authorizationRepository;
	@Autowired
	public AuthorizationQueryService(AuthorizationRepository authorizationRepository) {
		this.authorizationRepository=authorizationRepository;
	}
	@Override
	public AuthorizationShowDTO signIn(String email, String password) {
		Authorization authorization=authorizationRepository.getAuthorizationByEmail(email);
		Boolean signInSucess =authorization!=null && authorization.getPassword()!=null ?AuthorizationUtil.passwordMatches(password, authorization.getPassword()):Boolean.FALSE;
		return signInSucess? authorization.toAuthorizationSignInDTO():new AuthorizationShowDTO();
	}


}
