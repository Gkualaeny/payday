package com.paydayauthorization.paydayauthorization.model.authorization;

public interface IAuthorizationQueryService {

	AuthorizationShowDTO signIn(String email,String password);

}
