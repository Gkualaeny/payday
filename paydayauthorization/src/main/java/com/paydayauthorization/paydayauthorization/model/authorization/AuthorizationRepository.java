package com.paydayauthorization.paydayauthorization.model.authorization;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AuthorizationRepository  extends JpaRepository<Authorization, Long> {
	@Query("SELECT a FROM Authorization a  WHERE a.email = ?1")
	Authorization getAuthorizationByEmail(String email);



}
