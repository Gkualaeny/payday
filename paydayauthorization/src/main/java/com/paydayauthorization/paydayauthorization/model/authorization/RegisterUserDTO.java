package com.paydayauthorization.paydayauthorization.model.authorization;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import com.sun.istack.NotNull;

public class RegisterUserDTO {

    private String email;
    @NotNull
    @Pattern(regexp = "^[\\p{Alnum}]{1,32}$",message = "Only allow passwords alphanumeric characters")
    @Length(min = 6, message = "Only allow passwords with 6 or more")
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

}
