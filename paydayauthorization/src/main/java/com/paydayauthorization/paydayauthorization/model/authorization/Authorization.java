package com.paydayauthorization.paydayauthorization.model.authorization;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

@Entity
@Table(name = "authorization")
public class Authorization {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false,unique = true)
    private String email;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private String firstName;
    public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	@Column(nullable = false)
    private String lastName;
    private String phoneNumber;


    public AuthorizationDTO toDTO() {
    	AuthorizationDTO authorizationDTO = new AuthorizationDTO();
    	authorizationDTO.setEmail(getEmail());
    	authorizationDTO.setFirstName(getFirstName());
    	authorizationDTO.setLastName(getLastName());
    	authorizationDTO.setPassword(getPassword());
    	authorizationDTO.setPhoneNumber(getPhoneNumber());
    	authorizationDTO.setId(getId());
    	return authorizationDTO;
    }
    public AuthorizationShowDTO toAuthorizationSignInDTO() {
    	AuthorizationShowDTO authorizationDTO = new AuthorizationShowDTO();
    	authorizationDTO.setEmail(getEmail());
    	authorizationDTO.setFirstName(getFirstName());
    	authorizationDTO.setLastName(getLastName());
    	authorizationDTO.setPhoneNumber(getPhoneNumber());
    	authorizationDTO.setId(getId());
    	return authorizationDTO;
    }

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}





	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
