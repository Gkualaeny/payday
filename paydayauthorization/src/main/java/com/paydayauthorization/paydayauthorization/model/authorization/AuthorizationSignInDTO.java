package com.paydayauthorization.paydayauthorization.model.authorization;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class AuthorizationSignInDTO {
    @Pattern(regexp = "^[\\p{Alnum}]{1,32}$")
    @Size(min = 6, max = 50)
    @NotNull
    private String email;
    @NotNull
    private String password;

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
