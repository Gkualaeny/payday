package com.paydayauthorization.paydayauthorization.model.authorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paydayauthorization.paydayauthorization.util.AuthorizationUtil;


@Service
public class AuthorizationCommandService implements IAuthorizationCommandService {
	private AuthorizationRepository authorizationRepository;
	@Autowired
	public AuthorizationCommandService(AuthorizationRepository authorizationRepository) {
		this.authorizationRepository=authorizationRepository;
	}



	@Override
	public AuthorizationShowDTO signUp(RegisterUserDTO registerUserDTO) {
		Authorization authorization=new Authorization();
		authorization.setEmail(registerUserDTO.getEmail());
		authorization.setFirstName(registerUserDTO.getFirstName());
		authorization.setLastName(registerUserDTO.getLastName());
		authorization.setPassword(AuthorizationUtil.passwordEncoder(registerUserDTO.getPassword()));
		authorization.setPhoneNumber(registerUserDTO.getPhoneNumber());
		authorizationRepository.save(authorization);
		return authorizationRepository.save(authorization).toAuthorizationSignInDTO();
	}

}
