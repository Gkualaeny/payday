package com.paydayaccount.paydayaccount.model.account;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.*;

import com.paydayaccount.paydayaccount.enums.AccountStatusEnum;
import com.paydayaccount.paydayaccount.enums.AccountTypeEnum;

@Entity
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false/*, unique = true*/)
    private String accounNumber;
    @Column(nullable = false)
    @Enumerated
    private AccountTypeEnum accountType;
    private LocalDate dateOfCreation;
    private Long userId;
    @Column(nullable = false)
    @Enumerated
    private AccountStatusEnum accountStatus;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAccounNumber() {
		return accounNumber;
	}
	public void setAccounNumber(String accounNumber) {
		this.accounNumber = accounNumber;
	}
	public AccountTypeEnum getAccountType() {
		return accountType;
	}
	public void setAccountType(AccountTypeEnum accountType) {
		this.accountType = accountType;
	}

	public LocalDate getDateOfCreation() {
		return dateOfCreation;
	}
	public void setDateOfCreation(LocalDate dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

    public AccountDTO toDTO() {
    	AccountDTO accountDTO = new AccountDTO();
    		accountDTO.setAccocuntStatus(getAccountStatus());
    		accountDTO.setAccounNumber(getAccounNumber());
    		accountDTO.setAccountType(getAccountType());
    		accountDTO.setDateOfCreation(getDateOfCreation());
    		accountDTO.setId(getId());
    		accountDTO.setUserId(getUserId());
    	return accountDTO;
    }
	public AccountStatusEnum getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(AccountStatusEnum accountStatus) {
		this.accountStatus = accountStatus;
	}
}
