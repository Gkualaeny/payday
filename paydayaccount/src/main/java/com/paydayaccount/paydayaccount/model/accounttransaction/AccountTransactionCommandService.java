package com.paydayaccount.paydayaccount.model.accounttransaction;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paydayaccount.paydayaccount.enums.AccountStatusEnum;
import com.paydayaccount.paydayaccount.enums.AccountTransactionEnum;
import com.paydayaccount.paydayaccount.model.account.AccountDTO;
import com.paydayaccount.paydayaccount.model.account.OpenAccountDTO;


@Service
public class AccountTransactionCommandService implements IAccountTransactionCommandService {
	private AccountTransactionRepository accountTransactionRepository;
	@Autowired
	public AccountTransactionCommandService(AccountTransactionRepository accountTransactionRepository) {
		this.accountTransactionRepository=accountTransactionRepository;
	}

	@Override
	public AccountTransactionDTO insertAccountTransaction(AccountDTO accountDTO,OpenAccountDTO openAccountDTO) {
		AccountTransaction accountTransaction=new AccountTransaction();
		accountTransaction.setBalance(openAccountDTO.getBalance());
		accountTransaction.setBalanceCurrencyCode(openAccountDTO.getBalanceCurrencyCode());
		accountTransaction.setAccountId(accountDTO.getId());
		accountTransaction.setDateOfTransaction(LocalDate.now());
		accountTransaction.setDescription(AccountTransactionEnum.ACCOUNT_OPENED.toString());
		AccountTransaction result=accountTransactionRepository.save(accountTransaction);
		return result.toDTO();
	}


}
