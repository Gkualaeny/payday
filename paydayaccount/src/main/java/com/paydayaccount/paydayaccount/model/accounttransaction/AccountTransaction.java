package com.paydayaccount.paydayaccount.model.accounttransaction;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.*;

import com.paydayaccount.paydayaccount.enums.AccountStatusEnum;
import com.paydayaccount.paydayaccount.enums.AccountTypeEnum;

@Entity
@Table(name = "Account_transaction")
public class AccountTransaction {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false/*, unique = true*/)
    private Long accountId;
    private BigDecimal balance;
    private String balanceCurrencyCode;
    private LocalDate dateOfTransaction;
    private String description;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getBalanceCurrencyCode() {
		return balanceCurrencyCode;
	}
	public void setBalanceCurrencyCode(String balanceCurrencyCode) {
		this.balanceCurrencyCode = balanceCurrencyCode;
	}
    public AccountTransactionDTO toDTO() {
    	AccountTransactionDTO accountTransactionDTO = new AccountTransactionDTO();
    	accountTransactionDTO.setAccountId(getAccountId());
    	accountTransactionDTO.setBalance(getBalance());
    	accountTransactionDTO.setBalanceCurrencyCode(getBalanceCurrencyCode());
    	accountTransactionDTO.setDateOfTransaction(getDateOfTransaction());
    	accountTransactionDTO.setId(getId());
    	accountTransactionDTO.setDescription(getDescription());
    	return accountTransactionDTO;
    }
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public LocalDate getDateOfTransaction() {
		return dateOfTransaction;
	}
	public void setDateOfTransaction(LocalDate dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
