package com.paydayaccount.paydayaccount.model.accounttransaction;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class AccountTransactionQueryService implements IAccountTransactionQueryService {
	private AccountTransactionRepository accountTransactionRepository;
	
	@Autowired
	public AccountTransactionQueryService(AccountTransactionRepository accountTransactionRepository) {
		this.accountTransactionRepository=accountTransactionRepository;
	}

	@Override
	public List<AccountTransactionDTO> getAccountTransactionByAccountId(Long accountId) {
		List<AccountTransactionDTO> accountTransactionDTOList=new ArrayList<>();
		List<AccountTransaction> accountTransactionList=accountTransactionRepository.getAccountTransactionByAccountIdOrderByDateOfTransactionDesc(accountId);	
		convertFromModelListToDTOList(accountTransactionDTOList, accountTransactionList);
		return accountTransactionDTOList;
	}

	private void convertFromModelListToDTOList(List<AccountTransactionDTO> accountTransactionDTOList,
			List<AccountTransaction> accountTransactionList) {
		if(accountTransactionList!=null && !accountTransactionList.isEmpty())
		{
			for (AccountTransaction accountTransaction : accountTransactionList) {
				if(accountTransaction!=null)
				{
					accountTransactionDTOList.add(accountTransaction.toDTO());
				}
			}
		}
	}

	public List<AccountTransactionDTO> getAccountTransactionByAccountIdList(List<Long> accountIdList) {
		List<AccountTransactionDTO> accountTransactionDTOList=new ArrayList<>(); 
		if(accountIdList!=null &&! accountIdList.isEmpty()) {
			List<AccountTransaction> accountTransactionList=accountTransactionRepository.getAccountsSummaryByUserIdList(accountIdList);			
			convertFromModelListToDTOList(accountTransactionDTOList, accountTransactionList);
		}
		 return accountTransactionDTOList;
	}

	

}
