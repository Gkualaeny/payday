package com.paydayaccount.paydayaccount.model.account;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.paydayaccount.paydayaccount.enums.AccountStatusEnum;

public interface AccountRepository  extends JpaRepository<Account, Long> {

   
    @Query("SELECT a FROM Account a  WHERE a.userId = ?1 AND a.accountStatus =?2")
	List<Account> getActiveAccountsByUserId(Long userId,AccountStatusEnum status);

}
