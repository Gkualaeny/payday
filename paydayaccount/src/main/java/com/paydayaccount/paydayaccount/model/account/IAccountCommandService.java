package com.paydayaccount.paydayaccount.model.account;

public interface IAccountCommandService {
 public AccountDTO openAccount(OpenAccountDTO openAccountDTO);
}
