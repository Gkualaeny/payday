package com.paydayaccount.paydayaccount.model.account;

import java.util.List;

public interface IAccountQueryService {

	List<AccountDTO> getActiveAccountsByUserId(Long userId);

}
