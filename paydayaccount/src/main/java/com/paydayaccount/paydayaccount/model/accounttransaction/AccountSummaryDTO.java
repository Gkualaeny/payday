package com.paydayaccount.paydayaccount.model.accounttransaction;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.paydayaccount.paydayaccount.enums.AccountTypeEnum;

public class AccountSummaryDTO {

    private BigDecimal balance;
    private String balanceCurrencyCode;
    private String accounNumber;
    private AccountTypeEnum accountType;


	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getBalanceCurrencyCode() {
		return balanceCurrencyCode;
	}
	public void setBalanceCurrencyCode(String balanceCurrencyCode) {
		this.balanceCurrencyCode = balanceCurrencyCode;
	}

	public String getAccounNumber() {
		return accounNumber;
	}
	public void setAccounNumber(String accounNumber) {
		this.accounNumber = accounNumber;
	}
	public AccountTypeEnum getAccountType() {
		return accountType;
	}
	public void setAccountType(AccountTypeEnum accountType) {
		this.accountType = accountType;
	}
}
