package com.paydayaccount.paydayaccount.model.accounttransaction;

import java.util.List;

public interface IAccountTransactionQueryService {

	public List<AccountTransactionDTO> getAccountTransactionByAccountId(Long accountId);
}
