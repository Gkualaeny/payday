package com.paydayaccount.paydayaccount.model.account;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.paydayaccount.paydayaccount.enums.AccountStatusEnum;
import com.paydayaccount.paydayaccount.enums.AccountTypeEnum;

public class AccountDTO {
    private Long id;
    private String accounNumber;
    private AccountTypeEnum accountType;
    private LocalDate dateOfCreation;
    private Long userId;
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAccounNumber() {
		return accounNumber;
	}
	public void setAccounNumber(String accounNumber) {
		this.accounNumber = accounNumber;
	}
	public AccountTypeEnum getAccountType() {
		return accountType;
	}
	public void setAccountType(AccountTypeEnum accountType) {
		this.accountType = accountType;
	}
	public LocalDate getDateOfCreation() {
		return dateOfCreation;
	}
	public void setDateOfCreation(LocalDate dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public AccountStatusEnum getAccocuntStatus() {
		return accocuntStatus;
	}
	public void setAccocuntStatus(AccountStatusEnum accocuntStatus) {
		this.accocuntStatus = accocuntStatus;
	}
	private AccountStatusEnum accocuntStatus;
}
