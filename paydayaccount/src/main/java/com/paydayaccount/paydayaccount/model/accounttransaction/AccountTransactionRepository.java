package com.paydayaccount.paydayaccount.model.accounttransaction;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AccountTransactionRepository  extends JpaRepository<AccountTransaction, Long> {


	List<AccountTransaction> getAccountTransactionByAccountIdOrderByDateOfTransactionDesc(Long accountId);
	
	@Query("SELECT a FROM AccountTransaction a WHERE a.accountId IN(?1) GROUP BY a.accountId HAVING MAX(a.dateOfTransaction)>0")
	List<AccountTransaction> getAccountsSummaryByUserIdList(List<Long> accountIdList);
}
