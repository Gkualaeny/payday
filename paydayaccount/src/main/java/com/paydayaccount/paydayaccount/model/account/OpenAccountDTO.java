package com.paydayaccount.paydayaccount.model.account;

import java.math.BigDecimal;

import com.paydayaccount.paydayaccount.enums.AccountTypeEnum;
import com.sun.istack.NotNull;

public class OpenAccountDTO {
@NotNull
 private Long userId;
@NotNull
 private BigDecimal balance;
@NotNull 
private String balanceCurrencyCode;
@NotNull 
private AccountTypeEnum accountType;
public Long getUserId() {
	return userId;
}
public void setUserId(Long userId) {
	this.userId = userId;
}
public BigDecimal getBalance() {
	return balance;
}
public void setBalance(BigDecimal balance) {
	this.balance = balance;
}
public String getBalanceCurrencyCode() {
	return balanceCurrencyCode;
}
public void setBalanceCurrencyCode(String balanceCurrencyCode) {
	this.balanceCurrencyCode = balanceCurrencyCode;
}
public AccountTypeEnum getAccountType() {
	return accountType;
}
public void setAccountType(AccountTypeEnum accountType) {
	this.accountType = accountType;
}

}
