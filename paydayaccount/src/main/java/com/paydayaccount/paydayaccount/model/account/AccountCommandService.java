package com.paydayaccount.paydayaccount.model.account;

import java.text.SimpleDateFormat;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paydayaccount.paydayaccount.enums.AccountStatusEnum;
import com.paydayaccount.paydayaccount.model.accounttransaction.AccountTransactionCommandService;


@Service
public class AccountCommandService implements IAccountCommandService {
	private AccountRepository accountRepository;
	private AccountTransactionCommandService accountTransactionCommandService;
	@Autowired
	public AccountCommandService(AccountRepository accountRepository,AccountTransactionCommandService accountTransactionCommandService) {
		this.accountRepository=accountRepository;
		this.accountTransactionCommandService=accountTransactionCommandService;
	}

	@Override
	public AccountDTO openAccount(OpenAccountDTO openAccountDTO) {
		Account account=new Account();
		account.setUserId(openAccountDTO.getUserId());
		account.setAccountStatus(AccountStatusEnum.ACTIVE);
		account.setAccountType(openAccountDTO.getAccountType());
		account.setAccounNumber(generateAccountNumber());
		account.setDateOfCreation(LocalDate.now());
		Account result=accountRepository.save(account);
		accountTransactionCommandService.insertAccountTransaction(result.toDTO(),openAccountDTO);
		return result.toDTO();
	}

	private String generateAccountNumber() {
		return  String.valueOf(System.currentTimeMillis());
	}

}
