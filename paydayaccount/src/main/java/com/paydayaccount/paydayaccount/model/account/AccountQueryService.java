package com.paydayaccount.paydayaccount.model.account;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paydayaccount.paydayaccount.enums.AccountStatusEnum;

@Service
public class AccountQueryService implements IAccountQueryService {
	private AccountRepository accountRepository;
	@Autowired
	public AccountQueryService(AccountRepository accountRepository) {
		this.accountRepository=accountRepository;
	}
	@Override
	public List<AccountDTO> getActiveAccountsByUserId(Long userId) {
		List<AccountDTO> accountDTOList=new ArrayList<>();
		List<Account>activeAccountList =accountRepository.getActiveAccountsByUserId(userId,AccountStatusEnum.ACTIVE);
		if(activeAccountList!=null &&!activeAccountList.isEmpty()) {
			for (Account account : activeAccountList) {
				if(account!=null) {
					accountDTOList.add(account.toDTO());
				}
			}
		}
		return accountDTOList;
	}

}
