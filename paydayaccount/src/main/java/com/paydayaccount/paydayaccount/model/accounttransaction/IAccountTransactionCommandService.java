package com.paydayaccount.paydayaccount.model.accounttransaction;

import com.paydayaccount.paydayaccount.model.account.AccountDTO;
import com.paydayaccount.paydayaccount.model.account.OpenAccountDTO;

public interface IAccountTransactionCommandService {


 public AccountTransactionDTO insertAccountTransaction(AccountDTO accountDTO, OpenAccountDTO openAccountDTO);
}
