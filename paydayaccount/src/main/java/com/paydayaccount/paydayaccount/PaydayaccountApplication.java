package com.paydayaccount.paydayaccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class PaydayaccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaydayaccountApplication.class, args);
	}

}
