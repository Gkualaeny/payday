package com.paydayaccount.paydayaccount.response;

import java.util.List;

import com.paydayaccount.paydayaccount.model.accounttransaction.AccountTransactionDTO;

public class ResponseGetAccountTransactionByAccountId extends BaseApiResponse{

private List<AccountTransactionDTO> accountTransactionDTOList;

public List<AccountTransactionDTO> getAccountTransactionDTOList() {
	return accountTransactionDTOList;
}

public void setAccountTransactionDTOList(List<AccountTransactionDTO> accountTransactionDTOList) {
	this.accountTransactionDTOList = accountTransactionDTOList;
}
}