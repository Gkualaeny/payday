package com.paydayaccount.paydayaccount.response;

import java.util.List;

import com.paydayaccount.paydayaccount.model.accounttransaction.AccountSummaryDTO;

public class ResponseGetAccountSummaryByUserId  extends BaseApiResponse{

	private List<AccountSummaryDTO> accountSummaryDTOList ;

	public List<AccountSummaryDTO> getAccountSummaryDTOList() {
		return accountSummaryDTOList;
	}

	public void setAccountSummaryDTOList(List<AccountSummaryDTO> accountSummaryDTOList) {
		this.accountSummaryDTOList = accountSummaryDTOList;
	}


}

