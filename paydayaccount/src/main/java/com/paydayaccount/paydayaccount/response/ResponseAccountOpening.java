package com.paydayaccount.paydayaccount.response;

import com.paydayaccount.paydayaccount.model.account.AccountDTO;

public class ResponseAccountOpening extends BaseApiResponse{

	private AccountDTO accountDTO;

	public AccountDTO getAccountDTO() {
		return accountDTO;
	}

	public void setAccountDTO(AccountDTO accountDTO) {
		this.accountDTO = accountDTO;
	}

}
