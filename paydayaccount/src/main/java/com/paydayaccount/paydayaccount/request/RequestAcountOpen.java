package com.paydayaccount.paydayaccount.request;
import com.paydayaccount.paydayaccount.model.account.OpenAccountDTO;

import io.swagger.annotations.ApiModel;


@ApiModel
public class RequestAcountOpen {
private OpenAccountDTO openAccountDTO;

public OpenAccountDTO getOpenAccountDTO() {
	return openAccountDTO;
}

public void setOpenAccountDTO(OpenAccountDTO openAccountDTO) {
	this.openAccountDTO = openAccountDTO;
}
}
