package com.paydayaccount.paydayaccount.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.paydayaccount.paydayaccount.model.account.AccountCommandService;
import com.paydayaccount.paydayaccount.model.account.AccountDTO;
import com.paydayaccount.paydayaccount.model.account.AccountQueryService;
import com.paydayaccount.paydayaccount.model.accounttransaction.AccountSummaryDTO;
import com.paydayaccount.paydayaccount.model.accounttransaction.AccountTransactionDTO;
import com.paydayaccount.paydayaccount.model.accounttransaction.AccountTransactionQueryService;
import com.paydayaccount.paydayaccount.request.RequestAcountOpen;
import com.paydayaccount.paydayaccount.response.ResponseAccountOpening;
import com.paydayaccount.paydayaccount.response.ResponseGetAccountSummaryByUserId;
import com.paydayaccount.paydayaccount.response.ResponseGetAccountTransactionByAccountId;
import com.sun.istack.NotNull;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/account")
@Api(value = "account")
public class AccountController {
	private AccountCommandService accountCommandService;
	private AccountQueryService accountQueryService;
	private AccountTransactionQueryService accountTransactionQueryService;
	@Autowired
	public AccountController(AccountCommandService accountCommandService,AccountTransactionQueryService accountTransactionQueryService,AccountQueryService accountQueryService) {
		this.accountCommandService=accountCommandService;
		this.accountQueryService=accountQueryService;
		this.accountTransactionQueryService=accountTransactionQueryService;
	}
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	/**/
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/get/account/transtaction/by/acount/id/{accountId}")
	public ResponseGetAccountTransactionByAccountId getAccountTransactionByAccountId(@PathVariable @NotNull Long accountId) {
		ResponseGetAccountTransactionByAccountId response=new ResponseGetAccountTransactionByAccountId();
		response.setAccountTransactionDTOList(accountTransactionQueryService.getAccountTransactionByAccountId(accountId));
		return response;
	}
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/get/account/summary/by/user/id/{userId}")
	public ResponseGetAccountSummaryByUserId getAccountSummaryByUserId(@PathVariable @NotNull Long userId) {
		ResponseGetAccountSummaryByUserId response=new ResponseGetAccountSummaryByUserId();
		List<AccountDTO> activeAccountDTOList=accountQueryService.getActiveAccountsByUserId(userId);
		List<Long> accountIdList = getAccountIdList(activeAccountDTOList);
		List<AccountTransactionDTO> accountTransactionDTOList=accountTransactionQueryService.getAccountTransactionByAccountIdList(accountIdList);
		List<AccountSummaryDTO> accountSummaryDTOList=new ArrayList<>();
		accountSummarySetter(accountSummaryDTOList,accountTransactionDTOList,activeAccountDTOList);
		response.setAccountSummaryDTOList(accountSummaryDTOList);
		return response;
	}
	private void accountSummarySetter(List<AccountSummaryDTO> accountSummaryDTOList,
			List<AccountTransactionDTO> accountTransactionDTOList, List<AccountDTO> activeAccountDTOList) {
		if(!accountTransactionDTOList.isEmpty()&&!activeAccountDTOList.isEmpty()) {
			for (AccountDTO accountDTO : activeAccountDTOList) {
				AccountSummaryDTO accountSummaryDTO=new AccountSummaryDTO();
				AccountTransactionDTO accountTransactionDTO=accountTransactionDTOList.stream().filter(x->x.getAccountId().equals(accountDTO.getId())).findFirst().orElse(new AccountTransactionDTO());
				accountSummaryDTO.setAccounNumber(accountDTO.getAccounNumber());
				accountSummaryDTO.setAccountType(accountDTO.getAccountType());
				accountSummaryDTO.setBalance(accountTransactionDTO.getBalance());
				accountSummaryDTO.setBalanceCurrencyCode(accountTransactionDTO.getBalanceCurrencyCode());
				accountSummaryDTOList.add(accountSummaryDTO);
			}
		}
		
	}
	private List<Long> getAccountIdList(List<AccountDTO> activeAccountDTOList) {
		List<Long> accountIdList=new ArrayList<>();
		if(activeAccountDTOList!=null && !activeAccountDTOList.isEmpty()) {
			for (AccountDTO accountDTO : activeAccountDTOList) {
				if(accountDTO!=null&&accountDTO.getId()!=null) {
				accountIdList.add(accountDTO.getId());
				}
			}
		}
		return accountIdList;
	}
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "", notes = "Get data of application screens")
	@PostMapping( value="/open/account", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseAccountOpening openAccount(@RequestBody @Valid RequestAcountOpen request) {
		AccountDTO accountDTO=accountCommandService.openAccount(request.getOpenAccountDTO());
		ResponseAccountOpening response=new ResponseAccountOpening();
		response.setAccountDTO(accountDTO);
		return response;
	}

}
